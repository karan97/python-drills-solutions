import functools


def memoize(func):
    look_up = {}
    @functools.wraps(func)
    def wrapper(*args,**kwargs):
        n = args[0]
        print(n, look_up)
        if n in look_up.keys():
            print('coming from lookup')
            return look_up[n]
        else:
            print('not coming from lookup')
            value = func(*args, **kwargs)
            look_up[n] = value
            print(look_up)
            return value
    return wrapper


@memoize
def fib(n):
    print("Computing fb({})".format(n))
    if n in [0, 1]:
        return n
    return fib(n - 1) + fib(n - 2)
