def add3(a, b, c):
    return sum([a, b, c])


def unpacking1():
    items = [1, 2, 3]
    return add3(*items)
    # Call add3 by passing unpacking items


def unpacking2():
    kwargs = {'a': 1, 'b': 2, 'c': 3}
    return add3(**kwargs)


def call_function_using_keyword_arguments_example():
    a = 1
    b = 2
    c = 3
    return add3(a, b, c)
    # Call add3 by passing a, b and c as keyword arguments


def add_n(*args):
    sum = 0
    for n in args:
        sum += n
    return sum


def add_kwargs(*, a, b):
    return a+b


def universal_acceptor(*args, **kwargs):
    return args, kwargs

print(unpacking1())
print(unpacking2())
print(call_function_using_keyword_arguments_example())
print(add_n(1, 2, 3, 4, 5, 6, 7))
print(add_kwargs(a=2, b=3))
print(universal_acceptor(1, 2, 3, a=4, b=5))
