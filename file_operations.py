def read_file(path):
    fs = open(path)
    return fs.read()


def write_to_file(path, s):
    fs = open(path, 'w')
    fs.write(s)
    fs.close()


def append_to_file(path, s):
    fs = open(path, 'a')
    fs.write(s)
    fs.close()


def numbers_and_squares(n, file_path):
    fs = open(file_path, 'w')
    for i in range(1, n+1):
        str1 = str(i)+',' + str(i**2)+'\n'
        fs.write(str1)
    fs.close()
