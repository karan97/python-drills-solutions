import re


def word_count(s):
    s = re.sub(r'[^\w\s-]', '', s)
    words = []
    for word in s.split():
        words.append((word, s.split().count(word)))
    return dict(list(dict.fromkeys(words).keys()))


def dict_items(d):
    list1 = []
    for key in d.keys():
        list1.append((key, d[key]))
    return list1


def dict_items_sorted(d):
    list1 = []
    for key in d.keys():
        list1.append((key, d[key]))
    return sorted(list1, key=lambda x: x[0])


# print(dict_items({'a': 1, 'b': 3, 'c': 5}))

if __name__ == "__main__":
    print("hello")
    result = word_count("hello a b c a")
    print(result);