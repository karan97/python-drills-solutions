import re


def html_dict_search_impl(html_dict, selector, matched_items):
    selector = re.sub(r'[^\w\s-]', '', selector)
    for child in html_dict['children']:
        items = html_dict_search_impl(child, selector, matched_items)
        if selector in child['attrs'].values():
            matched_items.append(child)
    return matched_items


def html_dict_search(html_dict, selector):
    matched_items = []
    return html_dict_search_impl(html_dict, selector, matched_items)
