"""
* Create a class called `Point` which has two instance variables,
`x` and `y` that represent the `x` and `y` co-ordinates respectively.

* Initialize these instance variables in the `__init__` method

* Define a method, `distance` on `Point` which accepts another `Point` object as an argument and
returns the distance between the two points.
"""
from math import sqrt


class Point:
    def __init__(self, x, y):
        self.x, self.y = x, y

    def distance(self, point):
        return sqrt((self.y-point.y)**2+(self.x-point.x)**2)
