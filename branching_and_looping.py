def integers_from_start_to_end_using_range(start, end, step):
    return list(range(start, end, step))


def integers_from_start_to_end_using_while(start, end, step):
    list1 = []
    if step < 0:
        while end < start:
            list1.append(start)
            start += step
    else:
        while start < end:
            list1.append(start)
            start += step
    return list1


def two_digit_primes():
    list1 = []
    for n in range(10, 100):
        for x in range(2, n):
            if n % x == 0:
                break
        else:
            list1.append(n)
    return list1
