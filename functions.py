def is_prime(n):
    for x in range(2, n):
        if n % x == 0:
            return False
    else:
        return True


def n_digit_primes(digit):
    list1 = []
    for n in range(10**(digit-1), 10**digit):
        if n == 1:
            continue
        if is_prime(n):
            list1.append(n)
    return list1


print(n_digit_primes(1))
