import math
import functools
import time


def timer(func):
    @functools.wraps(func)
    def wrapper_timer(x):
        start_time = time.perf_counter()
        value = func(x)
        end_time = time.perf_counter()
        run_time = end_time - start_time
        print(f"Finished {func.__name__!r} in {run_time:.10f} secs")
        return value
    return wrapper_timer


@timer
def sqrt(x):
    return math.sqrt(x)


@timer
def square(x):
    return x * x


# sqrt(234561)
# square(234561)
